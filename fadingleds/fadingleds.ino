// The variables to store the target color value (0-255)
int red, green, blue;

// The variables to store the current/temporary color value (0-255)
int curRed, curGreen, curBlue;

void setup() {
  Serial.begin(9600);
}

void loop() {
  // Set new random target color value if the current color value has reached the target color value
  if (curRed == red) red = random(0, 255);  
  if (curGreen == green) green = random(0, 255);
  if (curBlue == blue) blue = random(0, 255); 

  // Set current color step
  curRed = getColorStep(red, curRed);
  curGreen = getColorStep(green, curGreen);
  curBlue = getColorStep(blue, curBlue);

  // Debug output
  Serial.println("[DEBUG] Target color: R " + String(red) + " G " + String(green) + " B " + String(blue));
  Serial.println("[DEBUG] Current color: R " + String(curRed) + " G " + String(curGreen) + " B " + String(curBlue));
  Serial.println("=============================================");

  // Send color values to LED1
  analogWrite(D1, curRed);
  analogWrite(D2, curGreen);
  analogWrite(D3, curBlue);
  
  // Send color values to LED2
  //analogWrite(D4, curRed);
  //analogWrite(D5, curGreen);
  //analogWrite(D6, curBlue);

  // ...
  
  delay(30);
}

int getColorStep(int targetColor, int curColor) {
  if (curColor < targetColor) return ++curColor;
  else return --curColor;
}
